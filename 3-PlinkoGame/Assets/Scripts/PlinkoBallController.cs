using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlinkoBallController : MonoBehaviour
{
    public float speed = 5f;
    Rigidbody rb;
    bool controllable = true;
    public Light lgt;

    private float RLimit = 4.25f;
    private float Llimit = -4.25f;

    private bool EnteredZone = false;

    AudioSource audioSource;
    public AudioClip collisionClip;
    public AudioClip onSuccesClip;
    public AudioClip onFailedClip;
    public AudioClip onDropClip;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (controllable && InRange() && Input.GetKey(KeyCode.A))
        {
            transform.position += new Vector3(-speed * Time.deltaTime, 0, 0);
        }
        if (controllable && InRange() && Input.GetKey(KeyCode.D))
        {
            transform.position += new Vector3(speed * Time.deltaTime, 0, 0);
        }
        if (controllable && Input.GetKeyDown(KeyCode.Space))
        {
            audioSource.PlayOneShot(onDropClip);
            rb.isKinematic = false;
            controllable = false;
        }
        if (!InRange())
        {
            if (transform.position.x >= RLimit)
            {
                transform.position = new Vector3(transform.position.x - .25f, transform.position.y, transform.position.z);
            }
            else if (transform.position.x <= RLimit)
            {
                transform.position = new Vector3(transform.position.x + .25f, transform.position.y, transform.position.z);
            }
        }
      
    }

    private void OnTriggerEnter(Collider other)
    {
        // if (other.gameObject.name == ""Lose") No, name may change
        // if (other.gameObject.tag == "Lose") Not recommended
        if (other.gameObject.CompareTag("LoseZone"))
        {
            Debug.Log("You loose");
            EnteredZone = true;
            audioSource.PlayOneShot(onFailedClip);
        }
        else if (other.gameObject.CompareTag("WinZone"))
        {
            EnteredZone = true;
            audioSource.PlayOneShot(onSuccesClip);
            //UnityEngine.SceneManagement.SceneManager.LoadScene("PlinkoGameScene");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (!EnteredZone)
        {
            lgt.intensity = 2;
            //collision.gameObject.AddComponent<Light>().intensity = 1f;
            audioSource.PlayOneShot(collisionClip);
            //collision.gameObject.GetComponent<Light>().color = new Color(20, 13, 22, 0);
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        lgt.intensity = 1;
    }
    private bool InRange()
    {
        return transform.position.x <= RLimit && transform.position.x >= Llimit;
    }
}
