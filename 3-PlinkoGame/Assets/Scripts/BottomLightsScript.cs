using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomLightsScript : MonoBehaviour
{
    public Light lgt;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        lgt.range = 10;
        other.GetComponent<Light>().intensity = 0;
    }
}
