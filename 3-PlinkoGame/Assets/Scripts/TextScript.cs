using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextScript : MonoBehaviour
{
    public int myVar;
    public string myString = "hello";

    [SerializeField] float bleed;
    [HideInInspector] public int publicButNotPublic;

    public int myInt = 9;
    public PlayerData playerData;

    public BoxCollider boxCollider;
    BoxCollider boxCollider2;
    //public Collider BoxColliderStupidDontDoThis;


    private void Awake()
    {
        Debug.Log("Awake was called" + name);
        boxCollider2 = GetComponent<BoxCollider>();
        //BoxColliderStupidDontDoThis = GetComponent<Collider>(); //I'm overriting editor value
    }


    // Start is called before the first frame update
    void Start()
    {
        Debug.Log("Start was called" +name);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log("Update was called");
    }

    private void FixedUpdate()
    {
        //Debug.Log("Fixed update was called");
    }

    public void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision detected");
    }

    public void OnTriggerEnter(Collider other)
    {
        Debug.Log("TriggerEnter detected");
        //boxCollider.enabled = false;
        //GameObject.Destroy(other); //Only destroy the component
        GameObject.Destroy(other.gameObject);
    }

    public void OnDestroy()
    {
        Debug.Log("Destroyed");
    }

    [System.Serializable]
    public class PlayerData
    {
        public int hp;
        public string playerName;
        public int mana;
    }



}
