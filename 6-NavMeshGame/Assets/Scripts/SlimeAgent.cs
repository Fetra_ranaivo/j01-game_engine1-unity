using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SlimeAgent : MonoBehaviour
{
    HashSet<NavMeshAgent> navCollection;
    static int numberOfActiveSlimes = 0;
    const int SLIMES_MAX = 10;

    Vector2 timeToSpawn = new Vector2(5, 10);
    float timeOfSplit;


    private void Awake()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        navCollection = PlayerWithAnimBlendTree.GetNavMeshCollection();

        numberOfActiveSlimes++;
        timeOfSplit = Time.time + Random.Range(timeToSpawn.x, timeToSpawn.y);
    }

    // Update is called once per frame
    void Update()
    {
        navCollection = PlayerWithAnimBlendTree.GetNavMeshCollection();

        if (Time.time > timeOfSplit && numberOfActiveSlimes < SLIMES_MAX)
        {
            SplitSlime();
            timeOfSplit = Time.time + Random.Range(timeToSpawn.x, timeToSpawn.y);
        }


        if (PlayerWithAnimBlendTree.Instance)
        {
            foreach (NavMeshAgent nav in navCollection)
            {
                if (nav.enabled)
                    nav.SetDestination(PlayerWithAnimBlendTree.Instance.transform.position);
            }

        }
    }

    void SplitSlime()
    {
        GameObject newSlime = GameObject.Instantiate(gameObject);

        foreach (NavMeshAgent nav in navCollection)
        {
            if (nav.enabled)
                newSlime.GetComponent<NavMeshAgent>().Move(new Vector3(1, 0, 0)); //Moves it by an offset, its not a position set
        }
    }
}
