using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationPlayer : MonoBehaviour
{
    Animation anim; //Animation is an old component, it cannot play from event systems because it's Play function is not visible. We need to indirectly call through here
    //Animator is the component which has replaced animation, but its more complicated than needed for this simple Play call
    
    void Awake()
    {
        anim = GetComponent<Animation>();
    }

    // Update is called once per frame
    

    //This public function is called by the OnClick of the button, we linked it manually in the editor
    public void PlayAnim()
    {
        anim.Play();
    }
}
