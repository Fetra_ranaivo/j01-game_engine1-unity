using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class NavmeshPlayer : MonoBehaviour
{
    Camera mainCamera;
    public static NavmeshPlayer instance;
    NavMeshAgent navmeshAgent;
    
    // Start is called before the first frame update
    void Awake()
    {
        instance = this;
        mainCamera = GameObject.FindObjectOfType<Camera>(); //Super expensive function to find a component of type camera, can also use Camera.main, dont recommend it
        navmeshAgent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            RaycastHit rch;
            Ray mouseCameraRay = mainCamera.ScreenPointToRay(Input.mousePosition);   //Create a ray from camera mouse to world
            if(Physics.Raycast(mouseCameraRay, out rch))                              //We hit something
            {
                navmeshAgent.SetDestination(rch.point);                              //set destination to the thing we hit, probably the floor
            }

        }
    }
}
