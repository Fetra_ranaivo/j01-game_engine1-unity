using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class PlayerWithAnimBlendTree : MonoBehaviour
{
    //public static PlayerWithAnimBlendTree instance;

    Rigidbody rb;
    Animator anim;

    float accel = 17.5f;
    float maxSpeed = 25;

    float rotationVelo = 4;
    float jumpForce = 250;

    public GameObject healingParticle;
    public GameObject attackParticle;


    const float HP_MAX = 100;
    const float HP_DEFAULT = 85;
    float hp_Current;
    const float HP_DAMAGE_RATE = 10;
    const float RECOVERY_RATE = 5;
    const float HP_DECREASE_DURATION = .5f;

    const float WEAPON_IMPACT_DISTANCE = 25;
    const float WEAPON_REAL_FORCE = 800;
    float currentWeaponLoad;
    const float MAX_WEAPON_LOAD = 100;
    const float WEAPON_CONSOMMATION_RATE = 30;
    const float WEAPON_RECOVERY_RATE = 2f;
    const float WEAPON_LOAD_DECREASE_DURATION = 1.5f;
    float TIME_TO_WAIT_FOR_WEAPON_INCREASE = 3;

    AudioSource audioSrc;
    public AudioClip healedVoice;
    public AudioClip OnDamageVoice;
    public AudioClip particleSound;
    public AudioClip footSteps;
    public AudioClip goodMood;
    public AudioClip onAttackVoice;
    public AudioClip particleAttackSound;
    public AudioClip onJump;

    private static PlayerWithAnimBlendTree instance;
    public static PlayerWithAnimBlendTree Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<PlayerWithAnimBlendTree>();
            }
            return instance;
        }
    }


    static HashSet<NavMeshAgent> navCollection;
    NavMeshAgent[] nav;

    private void Awake()
    {
        instance = this;

        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();

        audioSrc = GetComponent<AudioSource>();

        navCollection = new HashSet<NavMeshAgent>();
        FindNavemeshAgent();

        hp_Current = HP_DEFAULT;

        currentWeaponLoad = MAX_WEAPON_LOAD;

    }

    private void FindNavemeshAgent()
    {
        nav = FindObjectsOfType<NavMeshAgent>();
        foreach (NavMeshAgent agent in nav)
        {
            navCollection.Add(agent);
        }
    }

    void Start()
    {

    }

    private void FixedUpdate()
    {
        rb.angularVelocity = Vector3.zero;

        if (Input.GetKey(KeyCode.W))
        {
            //audioSrc.PlayOneShot(footSteps);
            rb.velocity += transform.forward * accel * Time.fixedDeltaTime;
        }
        else if (Input.GetKey(KeyCode.S))
        {
            //audioSrc.PlayOneShot(footSteps);
            rb.velocity -= transform.forward * accel * Time.fixedDeltaTime;
        }

        if (Input.GetKey(KeyCode.A))
        {
            rb.angularVelocity = Vector3.up * -rotationVelo;
        }
        else if (Input.GetKey(KeyCode.D))
        {
            rb.angularVelocity = Vector3.up * rotationVelo;
        }

        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
        anim.SetFloat("Speed", rb.velocity.magnitude / maxSpeed); // vel magn = 8/ max vel = 8 => speed of 1
        anim.SetFloat("HP", hp_Current / HP_MAX);
    }

    void Update()
    {

        FindNavemeshAgent();

        if (Input.GetKeyDown(KeyCode.J))
        {
            audioSrc.PlayOneShot(onJump);
            anim.SetTrigger("isJumpTriggered");
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }


        if (Input.GetKey(KeyCode.H))
        {
            healingParticle.SetActive(true);
            anim.SetTrigger("isHealTriggered");
            StartCoroutine(IncreaseHealth());
        }

        if (Input.GetKeyUp(KeyCode.H))
        {
            StartCoroutine(WaitForParticleToSettle(4.0f));
        }

        if (Input.GetKeyDown(KeyCode.Space) && currentWeaponLoad > 25)
        {
            StartCoroutine(LaunchAttack()); //I use this because there's a strong anticipation on the animation so I delayed the effect by 1.25sec
        }

        if (Input.GetKeyUp(KeyCode.Space) && currentWeaponLoad > 25)
        {
            StartCoroutine(DecreaseWeaponLoadDuring(WEAPON_LOAD_DECREASE_DURATION));
            StartCoroutine(BeginIncreaseWeaponLoadAfter(TIME_TO_WAIT_FOR_WEAPON_INCREASE));
        }
    }


    IEnumerator BeginIncreaseWeaponLoadAfter(float sec)
    {
        yield return new WaitForSeconds(sec);
        while (currentWeaponLoad < MAX_WEAPON_LOAD)
        {
            currentWeaponLoad += WEAPON_RECOVERY_RATE * Time.deltaTime;
            currentWeaponLoad = Mathf.Clamp(currentWeaponLoad, 0, MAX_WEAPON_LOAD);
            yield return null;
        };
    }

    IEnumerator DecreaseWeaponLoadDuring(float sec)
    {
        float start = 0;
        while (start < sec)
        {
            start += Time.deltaTime;
            currentWeaponLoad -= WEAPON_CONSOMMATION_RATE * Time.deltaTime;
            yield return null;
        }
    }

    IEnumerator LaunchAttack()
    {
        anim.SetTrigger("isAttackTriggered");
        attackParticle.SetActive(true);
        yield return new WaitForSeconds(1.25f);

        Collider[] detectedColliders = Physics.OverlapSphere(transform.position, WEAPON_IMPACT_DISTANCE, LayerMask.GetMask("Scannable"));
        foreach (Collider detectedCollider in detectedColliders)
        {
            NavMeshAgent nav = detectedCollider.GetComponent<NavMeshAgent>();
            if (navCollection.Contains(nav))
            {
                nav.enabled = false;
            }

            if (detectedCollider.attachedRigidbody)
            {
                detectedCollider.gameObject.GetComponent<Rigidbody>().AddExplosionForce(AttackStrength(), transform.position, WEAPON_IMPACT_DISTANCE);
            }

            StartCoroutine(ReEnableNavMeshAgent());
        }

        audioSrc.PlayOneShot(onAttackVoice);
        attackParticle.SetActive(false);
        audioSrc.PlayOneShot(particleAttackSound);
    }

    private float AttackStrength()
    {
        //Weapon force is proportionnal to the percentage of available weapon load
        //As load increases, force increases because there is more to release
        float attack = Mathf.Lerp(0, WEAPON_REAL_FORCE, currentWeaponLoad / MAX_WEAPON_LOAD);
        return attack;
    }

    IEnumerator ReEnableNavMeshAgent()
    {

        yield return new WaitForSeconds(2);
        foreach (NavMeshAgent agent in navCollection)
        {
            if (!agent.enabled)
                agent.enabled = true;
        }
    }

    IEnumerator WaitForParticleToSettle(float timeToWait)
    {
        yield return new WaitForSeconds(timeToWait);
        healingParticle.SetActive(false);
    }

    IEnumerator IncreaseHealth()
    {
        audioSrc.PlayOneShot(particleSound);
        hp_Current += RECOVERY_RATE * Time.deltaTime;
        yield return null;

        yield return new WaitForSeconds(2);
        audioSrc.PlayOneShot(healedVoice);
    }


    private void OnCollisionEnter(Collision collision)
    {
        transform.Rotate(new Vector3(0, 0, 0));

        if (collision.gameObject.CompareTag("Enemy"))
        {
            StartCoroutine(DecreaseHealthFor(HP_DECREASE_DURATION));
            //anim.SetFloat("HP", hp_Current / HP_MAX);
        }
    }
    IEnumerator DecreaseHealthFor(float duration)
    {
        float start = 0;
        while (start < duration)
        {
            start += Time.deltaTime;
            hp_Current -= HP_DAMAGE_RATE * Time.deltaTime;
            yield return null;
        }

        audioSrc.PlayOneShot(OnDamageVoice);
    }

    public float GetCurrentHP()
    {
        return hp_Current;
    }

    public float GetMaxHP()
    {
        return HP_MAX;
    }

    public float GetCurrentWeaponLoad()
    {
        return currentWeaponLoad;
    }

    public float GetMaxWeaponLoad()
    {
        return MAX_WEAPON_LOAD;
    }

    public static HashSet<NavMeshAgent> GetNavMeshCollection()
    {
        return navCollection;
    }
}
