using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Slime : MonoBehaviour
{
    static int numberOfActiveSlimes = 0;
    static int SLIMES_MAX = 10;

    Vector2 timeToSpawn = new Vector2(10, 30);
    float timeOfSplit;
    NavMeshAgent agent;
    private void Awake()
    {
        //Slime comes alive, so increase the count
        numberOfActiveSlimes++;
        agent = GetComponent<NavMeshAgent>();
    }

    // Start is called before the first frame update
    void Start()
    {
        timeOfSplit = Time.time + Random.Range(timeToSpawn.x, timeToSpawn.y);

    }

    // Update is called once per frame
    void Update()
    {
        if (Time.time > timeOfSplit && numberOfActiveSlimes < SLIMES_MAX)
        {
            SplitSlime();
            timeOfSplit = Time.time + Random.Range(timeToSpawn.x, timeToSpawn.y);
        }
            
        
        if (Player.instance) //If player is not null, same as Player.instance != null, only works for Monobehaviours
        {
            agent.SetDestination(Player.instance.transform.position);
        }
        else if(NavmeshPlayer.instance)
        {
            agent.SetDestination(NavmeshPlayer.instance.transform.position);
        }
    

    }

    void SplitSlime()
    {
        GameObject newSlime = GameObject.Instantiate(gameObject); //Gameobject instantaite creates a clone and puts it in the scene, weither it is a prefab or a instance that already exists
        //It's awake will be called when it gets created
        //The problem is I spawn it inside myself, I need to spawn it next to me, however we cannot use transform.position IF it's a navmesh agent, so we move using that system
        newSlime.GetComponent<NavMeshAgent>().Move(new Vector3(1, 0, 0)); //Moves it by an offset, its not a position set
        
    }
}
