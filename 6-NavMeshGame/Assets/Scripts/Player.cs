using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public static Player instance;

    Rigidbody rb;
    Animator anim;
    float accel = 5;
    float maxSpeed = 8; 

    float dmgRate = 20f;
    float hp;
    float maxHP = 100;
    float rotationVelo = 2;
    // Start is called before the first frame update

    private void Awake()
    {
        instance = this; //Assuming only one player exits, it'll this and make the static variable point to it  Only works if one player exists.

        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();

        hp = maxHP;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.angularVelocity = Vector3.zero; //Reset it, so its only not zero when key is held
        
        if (Input.GetKey(KeyCode.W))
        {
          rb.velocity += transform.forward * accel * Time.deltaTime;
          //rb.AddForce(transform.forward * accelForce, ForceMode.Force); //ForceMode.Force is special! Doesnt require * Time.FixedDeltaTime
        }
        else if(Input.GetKey(KeyCode.S))
        {
            rb.velocity -= transform.forward * accel * Time.deltaTime;
        }
        
        if(Input.GetKey(KeyCode.A))
        {
            rb.angularVelocity = Vector3.up * -rotationVelo;
        }
        else if(Input.GetKey(KeyCode.D))
        {
            rb.angularVelocity = Vector3.up * rotationVelo;
        }

        if (Input.GetKey(KeyCode.O))
        {
            hp = Mathf.Clamp(hp + dmgRate * Time.deltaTime, 0, maxHP);
        }
        else if (Input.GetKey(KeyCode.L))
        {
            hp = Mathf.Clamp(hp - dmgRate * Time.deltaTime, 0, maxHP);
        }

        rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed); //Simple way to set max speed, however this is expensive and has an edge case (falling from the sky) that doesnt work

        anim.SetFloat("Speed", rb.velocity.magnitude / maxSpeed); 
        anim.SetFloat("HP", hp / maxHP);
    }
}
