using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuScript : MonoBehaviour
{
    public GameObject lifeBar;
    public GameObject weaponBar;
    
    void Start()
    {
        lifeBar.GetComponent<Image>().fillAmount = Mathf.Lerp(0, 1, PlayerWithAnimBlendTree.Instance.GetCurrentHP() / PlayerWithAnimBlendTree.Instance.GetMaxHP());
        weaponBar.GetComponent<Image>().fillAmount = Mathf.Lerp(0, 1, PlayerWithAnimBlendTree.Instance.GetCurrentWeaponLoad() / PlayerWithAnimBlendTree.Instance.GetMaxWeaponLoad());
    }

    void Update()
    {
        lifeBar.GetComponent<Image>().fillAmount = Mathf.Lerp(0, 1, PlayerWithAnimBlendTree.Instance.GetCurrentHP() / PlayerWithAnimBlendTree.Instance.GetMaxHP());
        lifeBar.GetComponent<Image>().color = Color.Lerp(Color.red, Color.green, PlayerWithAnimBlendTree.Instance.GetCurrentHP() / PlayerWithAnimBlendTree.Instance.GetMaxHP());


        weaponBar.GetComponent<Image>().fillAmount = Mathf.Lerp(0, 1, PlayerWithAnimBlendTree.Instance.GetCurrentWeaponLoad() / PlayerWithAnimBlendTree.Instance.GetMaxWeaponLoad());
        weaponBar.GetComponent<Image>().color = Color.Lerp(Color.red, Color.green, PlayerWithAnimBlendTree.Instance.GetCurrentWeaponLoad() / PlayerWithAnimBlendTree.Instance.GetMaxWeaponLoad());
    }
}
