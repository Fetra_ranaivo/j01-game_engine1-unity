using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleAnimator : MonoBehaviour
{
    Animator anim;
    float hp = .2f;
    float maxHp = 6;


    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        //anim.SetFloat("HP", hp);
    }

    // Update is called once per frame
    void Update()
    {
        anim.SetBool("isWalking", Input.GetKey(KeyCode.Z));
        anim.SetBool("isRuning", Input.GetKey(KeyCode.R));

        if (Input.GetKey(KeyCode.S))
        {

        }
        if (Input.GetKey(KeyCode.K))
        {
            //hp = Mathf.Clamp(hp - Time.deltaTime, 0, maxHp);
            anim.SetTrigger("Died");
           
        }
        if (Input.GetKey(KeyCode.H))
        {
            hp = Mathf.Clamp(hp + Time.deltaTime, 0, maxHp);
        }

        anim.SetFloat("HP", hp / maxHp);
        
    }
}
