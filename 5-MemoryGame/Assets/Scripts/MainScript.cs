using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainScript : MonoBehaviour
{
    GameObject resourceLoadedButton;

    float timeToSubmit = 3;
    float timeLeftToSubmit;

    bool canSubmit = false;
    public GameObject submitBtn;
    public GameObject timerBtn; //or Text btnText

    public Transform greenContent;
    public Transform redContent;

    Dictionary<GameObject, bool> buttonCollection;
    Dictionary<GameObject, Color> realColor;

    bool waitForNextRound = false;

    AudioSource audioSrc;
    public AudioClip click;
    public AudioClip success;
    public AudioClip error;
    public AudioClip startSubmit;

    bool coroutineEnded = false;

    bool win = false;
    bool lose = false;

    bool onStartGame = true;
    int buttonToCreate;
    const int BUTTON_TO_SPAWN = 2;

    private void Start()
    {
        audioSrc = GetComponent<AudioSource>();
        resourceLoadedButton = Resources.Load<GameObject>("Buttons/Button");

        buttonCollection = new Dictionary<GameObject, bool>();
        realColor = new Dictionary<GameObject, Color>();

        InstantiateButtons();
        onStartGame = false;
        RandomizeButtons();
        EnableSubmitButton(false);

        timeLeftToSubmit = timeToSubmit;
    }

    private void Update()
    {
        timeLeftToSubmit -= Time.deltaTime;

        if (NotTimeToSubmit())
            timerBtn.GetComponent<Text>().text = string.Format("{0:N0}", timeLeftToSubmit);

        if (TimeToSubmit())
            UpdateGameToSubmitState();

        if (waitForNextRound)
        {
            EnableSubmitButton(false);
            EnableGameButtons(false);
            if (!coroutineEnded)
                StartCoroutine(UpdateSubmitButtonColor());
        }

        if (win)
        {
            InstantiateButtons();
            RandomizeButtons();
            ResetGameState();
        }

        else if (lose)
        {
            InstantiateButtons();
            RandomizeButtons();
            ResetGameState();
        }

    }

    private void InstantiateButtons()
    {
        if (win || onStartGame)
        {
            buttonToCreate += BUTTON_TO_SPAWN;
            DestroyAllGameButtons();
            ClearButtonCollection();
            AddButtonsToScene();
        }

        if (lose)
        {
            //DO NOT SPAWN ADDITIONAL BUTTONS
            DestroyAllGameButtons();
            ClearButtonCollection();
            AddButtonsToScene();
        }

        PutButtonsInsideLeftScrollView();
    }

    private void RandomizeButtons()
    {
        foreach (KeyValuePair<GameObject, bool> button in buttonCollection)
        {
            button.Key.GetComponent<Button>().enabled = false; //False until count down ends (2s)
            RandomizeColors(button.Key);
            realColor.Add(button.Key, button.Key.GetComponent<Image>().color); //Keep track of thid button real color
            button.Key.GetComponentInChildren<Text>().text = string.Format("{0:N0}", Random.Range(0, 100)); //In this case: Previous colors hold their number, only new changes
            //Use Text btnText and use Random.Range(0,100).ToString;
        }
    }

    private void ResetGameState()
    {
        timeLeftToSubmit = 3;

        win = false;
        lose = false;

        coroutineEnded = false;
    }

    IEnumerator UpdateSubmitButtonColor()
    {
        submitBtn.GetComponent<Image>().color += new Color(.5f * Time.deltaTime, .5f * Time.deltaTime, .5f * Time.deltaTime, .5f * Time.deltaTime);
        yield return null;

        if (submitBtn.GetComponent<Image>().color.r >= 1.0f && submitBtn.GetComponent<Image>().color.g >= 1.0f && submitBtn.GetComponent<Image>().color.b >= 1.0f)
        {
            submitBtn.GetComponent<Image>().color = Color.white;
            coroutineEnded = true;
        }

        if (coroutineEnded)
            waitForNextRound = false;
    }


    private void UpdateGameToSubmitState()
    {
        audioSrc.PlayOneShot(startSubmit);
        submitBtn.GetComponent<Image>().color = Color.white;
        foreach (KeyValuePair<GameObject, bool> button in buttonCollection)
        {
            button.Key.GetComponent<Image>().color = Color.white;
        }
        canSubmit = true;
        EnableSubmitButton(true); //and wait for OnSubmitBtn() which return win or lose state
        EnableGameButtons(true);
    }


    private bool TimeToSubmit()
    {
        return timeLeftToSubmit <= 0 && !canSubmit && !waitForNextRound;
    }

    private bool NotTimeToSubmit()
    {
        return timeLeftToSubmit > 0;
    }

    private void EnableGameButtons(bool choice)
    {
        foreach (KeyValuePair<GameObject, bool> button in buttonCollection)
        {
            button.Key.GetComponent<Button>().enabled = choice;
        }
    }

    private void EnableSubmitButton(bool choice)
    {
        submitBtn.GetComponent<Button>().enabled = choice;
    }


    public void OnClickButton(GameObject button)
    {
        if (ButtonOnTheLeft(button))
            SetButtonToTheRight(button);
 
        else if (ButtonOnTheRight(button))
            SetButtonToTheLeft(button);
    }

    private void SetButtonToTheLeft(GameObject button)
    {
        audioSrc.PlayOneShot(click);
        button.transform.SetParent(greenContent, false);
        buttonCollection[button] = true; //Now on the left side //no need anymore

        Debug.Log("Button on LEFT");
    }

    private bool ButtonOnTheRight(GameObject button)
    {
        //return buttonCollection[button] == false;
        return button.transform.parent == redContent; //MODIF
    }


    private void SetButtonToTheRight(GameObject button)
    {
        audioSrc.PlayOneShot(click);
        button.transform.SetParent(redContent, false);
        buttonCollection[button] = false; //Now on the right side //no need anymore

        Debug.Log("Button on RIGHT");
    }


    private bool ButtonOnTheLeft(GameObject button)
    {
        //return buttonCollection[button] == true;
        return button.transform.parent == greenContent; //MODIF
    }


    public void OnSubmitBtn()
    {
        int buttonsInCorrectZoneCount = 0;
        foreach (KeyValuePair<GameObject, bool> button in buttonCollection)
        {
            buttonsInCorrectZoneCount = VerifyZone(buttonsInCorrectZoneCount, button);
        }
        if (buttonsInCorrectZoneCount == buttonCollection.Count)
            OnSuccess();
        else
            OnFailed();
    }


    private int VerifyZone(int count, KeyValuePair<GameObject, bool> button)
    {
        if (ButtonInCorrectZone(button.Key))
            count++;
        return count;
    }


    private void OnSuccess()
    {
        audioSrc.PlayOneShot(success);
        Debug.Log("All correct");
        submitBtn.GetComponent<Image>().color = Color.green;
        RevealButtonsRealColors();

        waitForNextRound = true;
        canSubmit = false;
        win = true;
        lose = false;
    }


    private void OnFailed()
    {
        audioSrc.PlayOneShot(error);
        Debug.Log("Incorrect");
        submitBtn.GetComponent<Image>().color = Color.red;
        RevealButtonsRealColors();

        waitForNextRound = true;
        canSubmit = false;
        lose = true;
        win = false;
    }


    private void RevealButtonsRealColors()
    {
        foreach (KeyValuePair<GameObject, bool> button in buttonCollection)
        {
            button.Key.GetComponent<Image>().color = realColor[button.Key];
        }
    }

   

    private void PutButtonsInsideLeftScrollView()
    {
        foreach (KeyValuePair<GameObject, bool> button in buttonCollection)
        {
            button.Key.transform.SetParent(greenContent, false); //to do: can be on instantiate
            button.Key.GetComponent<Button>().onClick.AddListener(() => OnClickButton(button.Key));
        }
    }

    private void ClearButtonCollection()
    {
        buttonCollection.Clear();
        realColor.Clear();
    }

    private void AddButtonsToScene()
    {
        for (int i = 0; i < buttonToCreate; i++)
        {
            buttonCollection.Add(GameObject.Instantiate(resourceLoadedButton), true); //true means button is on the left scrollView
            //to do: Set the parent using instantiate
            //buttonCollection.Add(GameObject.Instantiate(resourceLoadedButton, greenContent)
        }
    }

    private void DestroyAllGameButtons()
    {
        foreach (KeyValuePair<GameObject, bool> button in buttonCollection)
        {
            GameObject.Destroy(button.Key);
        }
    }

    private Color GetRandomColor()
    {
        return (Random.value > .5f) ? Color.red : Color.green; //50-50% CHANCE
    }
    private /*static*/ void RandomizeColors(GameObject button)
    {
        //int red = (int)Random.Range(0, 1.1f);
        //int green;
        //if (red < .5)
        //{
        //    red = 0;
        //    green = 1;
        //}
        //else
        //{
        //    red = 1;
        //    green = 0;
        //}

        //button.GetComponent<Image>().color = new Color(Random.value, Random.value, 0, 1);
        button.GetComponent<Image>().color = GetRandomColor();

    }


    private bool ButtonInCorrectZone(GameObject button)
    {
        if (ButtonIsRedAndOnTheRight(button) || ButtonIsGreenAndOnTheLeft(button))
            return true;
        return false;
    }

    private bool ButtonIsGreenAndOnTheLeft(GameObject button)
    {
        return button.transform.IsChildOf(greenContent) && realColor[button] == Color.green;
    }

    private bool ButtonIsRedAndOnTheRight(GameObject button)
    {
        return button.transform.IsChildOf(redContent) && realColor[button] == Color.red;
    }


}
