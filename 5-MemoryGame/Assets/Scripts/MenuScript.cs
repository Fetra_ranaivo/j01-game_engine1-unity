using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour
{
    public void OnStart()
    {
        Debug.Log("Start");
        SceneManager.LoadScene("MemoryGame", LoadSceneMode.Additive);
    }

    public void OnSettings()
    {
        Debug.Log("Settings");

    }

    public void OnApply()
    {
        Debug.Log("Apply");
    }

    public void OnCancel()
    {
        Debug.Log("Cancel");
    }
}
