using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{

    public GameObject speedValue;
    public GameObject rotationValue;

    public GameObject fuelAmount;


    void Start()
    {
        //var v = fuelAmount;
        //var vv = v.GetComponent<Image>();
        //var vvv = SpaceShip.Instance;

        fuelAmount.GetComponent<Image>().fillAmount = Mathf.Lerp(0, 1, SpaceShip.Instance.GetCurrentFuelAmount() / SpaceShip.Instance.GetMaxFuelAmount());

    }

    IEnumerator UpdateUIFuelAmount()
    {
        if (SpaceShip.Instance)
        {
            fuelAmount.GetComponent<Image>().fillAmount = Mathf.Lerp(0, 1, SpaceShip.Instance.GetCurrentFuelAmount() / SpaceShip.Instance.GetMaxFuelAmount());
            fuelAmount.GetComponent<Image>().color = Color.Lerp(Color.red, Color.green, SpaceShip.Instance.GetCurrentFuelAmount() / SpaceShip.Instance.GetMaxFuelAmount());
            yield return null;
        }
    }


    void Update()
    {

        StartCoroutine(UpdateUIFuelAmount());

        UpdateUISpeed();
        UpdateUIRotation();

        UIOnDanger();

    }

    private void UIOnDanger()
    {
        if (SpaceShip.Instance)
        {
            if (SpaceShip.Instance.DangerLandingAngles())
                rotationValue.GetComponent<Text>().color = Color.red;

            else
                rotationValue.GetComponent<Text>().color = Color.green;


            if (!SpaceShip.Instance.OnSafeLandingSpeed())
                speedValue.GetComponent<Text>().color = Color.red;

            else
                speedValue.GetComponent<Text>().color = Color.green;
        }
    }

    private void UpdateUIRotation()
    {
        if (SpaceShip.Instance)
        {
            rotationValue.GetComponent<Text>().text = SpaceShip.Instance.GetRotation().ToString();
        }
    }

    private void UpdateUISpeed()
    {
        if (SpaceShip.Instance)
        {
            speedValue.GetComponent<Text>().text = SpaceShip.Instance.GetSpeed().ToString();
        }
    }
}
