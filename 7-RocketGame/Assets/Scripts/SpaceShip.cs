using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpaceShip : MonoBehaviour
{
    Rigidbody2D rb;

    public float thrustForce = 15;
    public float turnForce = .1f;

    float safeLandingSpeed = 3;
    float safeLandingAngle = 20;

    private static SpaceShip instance;
    public static SpaceShip Instance
    {
        get
        {
            if (instance == null)
            {
                instance = GameObject.FindObjectOfType<SpaceShip>();
            }
            return instance;
        }
    }

    float maxFuelAmount = 100;
    float currentFuelAmount;
    const float FUEL_CONSOMMATION_RATE = 4;

  
    public GameObject life0;
    public GameObject life1;
    public GameObject life2;

    Vector3 initialPosition;
    Vector3 initialAngles;

    AudioSource audioSrc;
    public AudioClip onSucces;
    public AudioClip onFailed;

    void Start()
    {
        instance = this;
        rb = GetComponent<Rigidbody2D>();

        audioSrc = GetComponent < AudioSource > ();

        currentFuelAmount = maxFuelAmount;

        initialPosition = transform.position;
        initialAngles = transform.localEulerAngles;
    }
    void Update()
    {
        currentFuelAmount -= FUEL_CONSOMMATION_RATE * Time.deltaTime;
    }
    private void FixedUpdate()
    {

        if (Input.GetKey(KeyCode.W))
        {
            rb.AddForce(transform.up * thrustForce);
        }

        if (Input.GetKey(KeyCode.A))
        {
            rb.AddTorque(turnForce);
        }

        if (Input.GetKey(KeyCode.D))
        {
            rb.AddTorque(-turnForce);
        }

        //Debug.Log("Speed = " + rb.velocity.magnitude + ", Angle = " + transform.eulerAngles.z);

        if (currentFuelAmount <= 0)
        {
            rb.gravityScale = 2;
            GameObject.Destroy(life0);
            GameObject.Destroy(life1);
            GameObject.Destroy(life2);
            GameObject.Destroy(gameObject);
        }

    }

    public float GetCurrentFuelAmount()
    {
        return currentFuelAmount;
    }

    public float GetMaxFuelAmount()
    {
        return maxFuelAmount;
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (OnSafeLand(collision))
        {
            if (!DangerLandingAngles() && OnSafeLandingSpeed())
            {
                audioSrc.PlayOneShot(onSucces);
                Debug.Log("GOOD  JOB");
            }
        }

        else
        {
            UpdateLives();
            //audioSrc.PlayOneShot(onFailed);
        }
    }

    private void UpdateLives()
    {
        if (life0 != null)
        {
            GameObject.Destroy(life0);
            ResetPosition();
        }
        else if (life0 == null)
        {
            if (life1 != null)
            {
                GameObject.Destroy(life1);
                ResetPosition();
            }
            else if (life1 == null)
            {
                if (life2 != null)
                {
                    GameObject.Destroy(life2);
                    GameObject.Destroy(gameObject);
                }
            }
            
        }
        
    }

    private void ResetPosition()
    {
        transform.position = initialPosition;
        transform.rotation = Quaternion.Euler(initialAngles);
        currentFuelAmount = maxFuelAmount ;


    }
    private static bool OnSafeLand(Collision2D collision)
    {
        return collision.collider.gameObject.layer == LayerMask.NameToLayer("SafeLand");
    }

    public bool OnSafeLandingSpeed()
    {
        return rb.velocity.magnitude < safeLandingSpeed;
    }

    public bool DangerLandingAngles()
    {
        return GetLeftRotation() > safeLandingAngle && GetRightRotation() < 360 - safeLandingAngle;
    }

    public float GetSpeed()
    {
        return rb.velocity.magnitude;
    }

    public float GetRotation()
    {
        if (Mathf.Sin(transform.localEulerAngles.z) < 0)
            return transform.localEulerAngles.z - 360;
        else
            return transform.localEulerAngles.z;
    }

    public float GetLeftRotation()
    {
        return transform.localEulerAngles.z;
    }

    public float GetRightRotation()
    {
        return Mathf.Abs(transform.localEulerAngles.z);
    }

    public Rigidbody2D GetRigidbody2D()
    {
        return rb;
    }
}
