using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerSpawner : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject playerResourceInRAM = Resources.Load<GameObject>("Player/CubeBoy");
        GameObject playerObjectInScene = GameObject.Instantiate(playerResourceInRAM);
        playerObjectInScene.GetComponent<Player>().Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
