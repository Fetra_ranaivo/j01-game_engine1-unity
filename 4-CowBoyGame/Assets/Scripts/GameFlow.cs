using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameFlow : MonoBehaviour
{
    public GameObject ball;
    public GameObject player1;
    public GameObject player2;

    public AudioClip bipAudioClip;
    public AudioClip shotAudioClip;
    public AudioClip player1WinAudioClip;
    public AudioClip player2WinAudioClip;
    public AudioClip backgroundAmbiance;
    public AudioClip errorClip;

    AudioSource audioSource;
   
    //private Rigidbody rb;
    public Vector2 timeToSpawn = new Vector2(1, 3);
    private float timeLeftToSpawn;

    private bool timerEnd = false;
    private bool shot = false;
    private bool waitForNextGame = false;

    private bool player1Lose = false;
    private bool player2Lose = false;

    public float timeToRestartGame = 5;
    private float timeLeftToRestartGame;

    private float shotMomentum = 50;

    private string keypressed;

    private void Awake()
    {
        audioSource = GetComponent<AudioSource>();
    }

    void Start()
    {
        //rb = ball.GetComponent<Rigidbody>();
        timeLeftToSpawn = GetRandomTimeSpawn();
        timeLeftToRestartGame = timeToRestartGame;
    }

    void Update()
    {
        //rb.isKinematic = false;

        if (!timerEnd && !APlayerLose())
        {
            StartTimer(ref timerEnd);
        }

        if (!APlayerLose())
        {
            if (timerEnd && !shot)
            {
                WaitForShot();
            }
        }

        if (shot && !waitForNextGame)
        {
            //backgroundMusic.Stop();

            if (player1Lose)
            {
                Debug.Log("Player 2 wins!");
                audioSource.PlayOneShot(player2WinAudioClip);
            }
            else if (player2Lose)
            {
                Debug.Log("Player 1 wins!");
                audioSource.PlayOneShot(player1WinAudioClip);
            }
            waitForNextGame = true;
        }

        if (waitForNextGame)
        {
            timeLeftToRestartGame -= Time.deltaTime;
            Debug.Log(timeLeftToRestartGame);
            if (timeLeftToRestartGame <= 0)
            {
                ResetTimers();
                ResetPlayers();
                waitForNextGame = false;
            }
        }

        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit();
        }
    }

    private void ResetTimers()
    {
        timeLeftToSpawn = GetRandomTimeSpawn();
        timerEnd = false;

        timeToRestartGame = 5;
        timeLeftToRestartGame = timeToRestartGame;
    }

    private void ResetPlayers()
    {
        shot = false;

        ball.GetComponent<MeshRenderer>().material.color = new Color(1, 0, 0);

        player1.transform.position = new Vector3(-5, 0, 0);
        player1.transform.Rotate(new Vector3(0,0,0));
        player1.GetComponentInChildren<MeshRenderer>().material.color = new Color(1, 0.4377476f, 0.08867919f);
        player1Lose = false;

        player2.transform.position = new Vector3(5, 0, 0);
        player1.transform.Rotate(new Vector3(0,0,0));
        player2.GetComponentInChildren<MeshRenderer>().material.color = new Color(0.08522601f, 0.1308666f, 0.7924528f);
        player2Lose = false;
    }

    private void StartTimer(ref bool timerEnd)
    {
        audioSource.PlayOneShot(backgroundAmbiance);
        timeLeftToSpawn -= Time.deltaTime;
        CheckIfPlayerHitKeyBeforeTimerEnds();
        if (timeLeftToSpawn <= 0)
        {
            ball.GetComponent<MeshRenderer>().material.color = new Color(0, 1, 0);
            switchAudioClip();
            audioSource.PlayOneShot(bipAudioClip);
            timerEnd = true;
        }
    }

    private void CheckIfPlayerHitKeyBeforeTimerEnds()
    {
        keypressed = Input.inputString;

        if (keypressed == "a" && timeLeftToSpawn > 0)
        {
            shot = true;
            player1Lose = true;
            switchAudioClip();
            audioSource.PlayOneShot(errorClip);
        }

        if (keypressed == "l" && timeLeftToSpawn > 0)
        {
            shot = true;
            player2Lose = true;
            switchAudioClip();
            audioSource.PlayOneShot(errorClip);
        }
    }

    private void switchAudioClip()
    {
        audioSource.Stop();
        audioSource.Play();
    }

    private void WaitForShot()
    {
        keypressed = Input.inputString;
        if (keypressed == "a")
        {
            shot = true;
            audioSource.PlayOneShot(shotAudioClip);
            Player2Dies();
        }
        else if (keypressed == "l")
        {
            shot = true;
            audioSource.PlayOneShot(shotAudioClip);
            Player1Dies();
        }
    }

    private void Player2Dies()
    {
        player2.transform.position += new Vector3(shotMomentum * Time.deltaTime, 0, 0);
        player2.transform.Rotate(new Vector3(-35, 0, 0));
        player2.GetComponentInChildren<MeshRenderer>().material.color = new Color(0, 0, 0);
        player2Lose = true;
        
    }

    private void Player1Dies()
    {
        player1.transform.position += new Vector3(-shotMomentum * Time.deltaTime, 0, 0);
        player1.transform.Rotate(new Vector3(-35, 0, 0));
        player1.GetComponentInChildren<MeshRenderer>().material.color = new Color(0, 0, 0);
        player1Lose = true;
    }

    private float GetRandomTimeSpawn()
    {
        return Random.Range(timeToSpawn.x, timeToSpawn.y);
    }

    private bool APlayerLose()
    {
        return player1Lose || player2Lose;
    }
  

}
